Real code is not kept on the master branch.
See one of the other versioned branches instead:
7.x => Drupal 7
6.x => Drupal 6
